import { BUY_ICECREAM } from "./iceCreamType"
import { SELL_ICECREAM } from "./iceCreamType"

const initialState = {
    numOfIceCream: 20 
}

const iceCreamReducer = (state = initialState, action) => {
    switch(action.type) {
        case BUY_ICECREAM: return {
            ...state,
            numOfIceCream: state.numOfIceCream - action.payload
        }

        case SELL_ICECREAM: return {
            ...state,
            numOfIceCream: parseInt(state.numOfIceCream) + parseInt(action.payload)
        }

        default: return state
    }
}

export default iceCreamReducer