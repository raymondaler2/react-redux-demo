import { BUY_ICECREAM, SELL_ICECREAM } from "./iceCreamType"

export const buyIceCream = (number = 1)=>{
    return {
        type: BUY_ICECREAM,
        payload: number
    }
}

export const sellIceCream = (number = 1)=> {
    return {
        type: SELL_ICECREAM,
        payload: number
    }
}