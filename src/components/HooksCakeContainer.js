import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { buyCake, sellCake } from "../redux";

const HooksCakeContainer = () =>{
    const numOfCakes = useSelector(state => state.cake.numOfCakes)
    const dispatch = useDispatch()
    return (
        <div>
            <h2>Num of cakes HooksCakeContainer - {numOfCakes}</h2>
            <button onClick={()=>dispatch(buyCake())}>Buy cake</button>
            <button onClick={()=>dispatch(sellCake())}>Sell cake</button>
        </div>
    )
}

export default HooksCakeContainer