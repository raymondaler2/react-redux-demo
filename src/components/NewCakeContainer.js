import React, { useState } from "react";
import { connect } from "react-redux";
import { buyCake, sellCake } from "../redux";



const NewCakeContainer = (props) => {
    const [number, setNumbers] = useState(1)

    return(
        <div>
            <h2>Numer of Cakes - {props.numOfCakes}</h2>
            <input type='text' value={number} onChange={e => setNumbers(e.target.value)}/>
            <button onClick={() => props.buyCake(number)}>Buy {number} Cake</button>
            <button onClick={() => props.sellCake(number)}>Sell {number} Cake</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        numOfCakes: state.cake.numOfCakes
    }
}

const mapDispatchToProps = dispatch => {
    return {
        buyCake: number => dispatch(buyCake(number)),
        sellCake: number => dispatch(sellCake(number))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCakeContainer)