import React from "react";
import { connect } from "react-redux";
import { buyIceCream, sellIceCream} from "../redux";



const iceCreamContainer = (props) => {
    return(
        <div>
            <h2>Numer of Ice Cream - {props.numOfIceCream}</h2>
            <button onClick={()=>{
                props.buyIceCream()
                }}>Buy Ice Cream</button>
            <button onClick={()=>{
                props.sellIceCream()
                }}>Sell Ice Cream</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        numOfIceCream: state.iceCream.numOfIceCream
    }
}

const mapDispatchToProps = dispatch => {
    return {
        buyIceCream: () => dispatch(buyIceCream()),
        sellIceCream: () => dispatch(sellIceCream())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(iceCreamContainer)