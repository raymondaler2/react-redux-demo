import React, { useState } from "react";
import { connect } from "react-redux";
import { buyIceCream, sellIceCream} from "../redux";



const NewiceCreamContainer = (props) => {
    const [number, setNumber] = useState(1)

    return(
        <div>
            <h2>Numer of Ice Cream - {props.numOfIceCream}</h2>
            <input type='text' value={number} onChange={e => setNumber(e.target.value)}/>
            <button onClick={() => props.buyIceCream(number)}>Buy {number} Ice Cream</button>
            <button onClick={() => props.sellIceCream(number)}>Sell {number} Ice Cream</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        numOfIceCream: state.iceCream.numOfIceCream
    }
}

const mapDispatchToProps = dispatch => {
    return {
        buyIceCream: number => dispatch(buyIceCream(number)),
        sellIceCream: number => dispatch(sellIceCream(number))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewiceCreamContainer)