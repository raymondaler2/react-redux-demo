import React from "react";
import { connect } from "react-redux";
import { buyCake, sellCake } from "../redux";



const CakeContainer = (props) => {
    return(
        <div>
            <h2>Numer of Cakes - {props.numOfCakes}</h2>
            <button onClick={()=>{
                props.buyCake()
                }}>Buy Cake</button>
            <button onClick={()=>{
                props.sellCake()
                }}>Sell Cake</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        numOfCakes: state.cake.numOfCakes
    }
}

const mapDispatchToProps = dispatch => {
    return {
        buyCake: () => dispatch(buyCake()),
        sellCake: ()=> dispatch(sellCake())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CakeContainer)